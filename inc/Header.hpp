#pragma once
#define N 100
#define M 100
#define L 1000

namespace kk
{

	void vvod(int n, int a[L]);
	
	void vivod(int n, int a[L]);

	int proizv(int x);

	int summa(int x);

	void sort(int n, int a[L]);
}

namespace km
{
	void vvod(int n, int m, int** b);

	void vivod(int n, int m, int** b);

	int stolb(int n, int m, int** b);

	void zamena(int n, int m, int** b, int num);
}