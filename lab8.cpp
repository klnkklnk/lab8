﻿#include <iostream>
#include <cmath>
#include "inc/Header.hpp"

int main()
{
	setlocale(LC_ALL, "Russian");
	int o;
	std::cout << "Выберите задание (2 или 3): ";
	std::cin >> o;
	switch(o)
	{
		case 2:
		{
			int n;
			std::cout << "Введите размерность массива: ";
			std::cin >> n;
			int* a = new int[n];
			kk::vvod(n, a);
			kk::sort(n, a);
			std::cout << "Итоговый массив: " << std::endl;
			kk::vivod(n, a);
			delete[] a;
			return 0;
		}

		case 3:
		{
			int n, m;
			std::cout << "Введите размерность массива(N x M): ";
			std::cin >> n >> m;
			int** b = new int*[n];
			for (int i = 0; i < n; i++)
				b[i] = new int[m];
			
			km::vvod(n, m, b);
			km::zamena(n, m, b, km::stolb(n, m, b));
			km::vivod(n, m, b);
		
			for (int i = 0; i < n; i++)
				delete[] b[i];
			delete[] b;

			return 0;
		}
	}
	
}
