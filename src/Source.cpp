#include <iostream>
#include <cmath>
#include "../inc/Header.hpp"


namespace kk
{
	void vvod(int n, int a[L])
	{
		for (int i = 0; i < n; i++)
			std::cin >> a[i];
	}

	void vivod(int n, int a[L])
	{
		for (int i = 0; i < n; i++)
			std::cout << a[i] << " ";
	}

	int proizv(int x)
	{
		int p = 1;
		while (x != 0)
		{
			p *= x % 10;
			x /= 10;
		}
		return(p);
	}

	int summa(int x)
	{
		int p = 0;
		while (x != 0)
		{
			p += x % 10;
			x /= 10;
		}
		return(p);
	}

	void sort(int n, int a[L])
	{
		int i, j, tmp;
		for (i = 0; i < n - 1; i++)
		{
			for (j = i + 1; j < n; j++)
			{
				if (proizv(a[i]) > proizv(a[j]))
				{
					tmp = a[i];
					a[i] = a[j];
					a[j] = tmp;
				}

				else if (proizv(a[i]) == proizv(a[j]))
				{
					if (summa(a[i]) > summa(a[j]))
					{
						tmp = a[i];
						a[i] = a[j];
						a[j] = tmp;
					}

					else if (summa(a[i]) == summa(a[j]))
					{
						if (a[i] > a[j])
						{
							tmp = a[i];
							a[i] = a[j];
							a[j] = tmp;
						}
					}
				}
			}
		}
	}
}

namespace km
{
	void vvod(int n, int m, int** b)
	{
		for (int i = 0; i < n; i++)
			for (int j = 0; j < m; j++)
				std::cin >> b[i][j];
	}

	void vivod(int n, int m, int** b)
	{
		for (int i = 0; i < n; i++)
		{
			for (int j = 0; j < m; j++)
				std::cout << b[i][j] << " ";
			std::cout << std::endl;
		}
	}

	int stolb(int n, int m, int** b)
	{
		int k = 0, num = 0, kmin = 101;
		for (int j = 0; j < m; j++)
		{
			for (int i = 0; i < n; i++)
				if (b[i][j] % 10 == 3)
					k++;
			if (k < kmin)
			{
				kmin = k;
				num = j;
			}
			k = 0;
		}
		return num;
	}

	void zamena(int n, int m, int** b, int num)
	{
		for (int i = 0; i < n; i++)
			for (int j = 0; j < m; j++)
				if (j !=  num)
					b[i][j] = -17;
	}
}